import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styles: [
  ]
})
export class MovieComponent implements OnInit {

  movie: any;
  returnPage; string = '';
  searchText: string = '';

  constructor(public movieService: MoviesService,
              public router: ActivatedRoute) {
  
      this.router.params.subscribe( resp => {
        console.log( resp );
        this.returnPage = resp['page'];

        if ( resp['searchText'] ) {
          this.searchText = resp['searchText'];
        }


        
        if( resp['id'] ){
          this.movie = this.movieService.getPelicula( resp ['id'] ).subscribe(rpt => {
            console.log( rpt );
            this.movie = rpt;
          });
        }
      });
  }

  ngOnInit(): void {
  }

}

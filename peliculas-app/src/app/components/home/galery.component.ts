import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-galery',
  templateUrl: './galery.component.html',
  styles: [
  ]
})
export class GaleryComponent implements OnInit {

  @Input('input_movies') cartelera;
  @Input('titulo') titulo_peli; 

  constructor() { }

  ngOnInit(): void {
  }

}

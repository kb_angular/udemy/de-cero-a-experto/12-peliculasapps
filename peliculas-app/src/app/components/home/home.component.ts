import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // No lo inicializo, para usar despues de que haya cargado la data
  movies_cartelera: any[];
  movies_populares: any[];
  movies_ninos    : any[];

  rutaImage = "image.tmdb.org/t/p/w300";

  constructor(private moviesService: MoviesService) { 
    
    this.moviesService.getCartelera().subscribe( res => {
      this.movies_cartelera = res;
      // console.log( this.movies_cartelera );
    });

    this.moviesService.getPopulares().subscribe( res => {
      this.movies_populares = res;
      // console.log( this.movies_cartelera );
    });

    this.moviesService.getPopularesNinos().subscribe( res => {
      this.movies_ninos = res;
    });

  }

  ngOnInit(): void {
    
  }

 

}

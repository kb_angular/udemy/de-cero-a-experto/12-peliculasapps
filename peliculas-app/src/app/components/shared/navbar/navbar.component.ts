import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // Enviamos la variables con Router
  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  searchMovie( textMovie: string ) {

    if ( textMovie.length === 0) {
      return;
    }
    
    this.router.navigate( ['search/', textMovie] );

  }

}

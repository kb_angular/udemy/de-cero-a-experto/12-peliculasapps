import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search: string = '';

  // recibimos la informacion un valor a traves de ActivateRoute
  constructor(public movieService: MoviesService,
              public router: ActivatedRoute) {
  
      this.router.params.subscribe( resp => {
        // console.log( resp );
        if( resp['text'] ){
          this.search = resp['text'];
          this.buscarPelicula();
        }
      });

  }

  ngOnInit(): void {
  }

  buscarPelicula() {
    if (this.search.length === 0 ){
      return;
    }

    this.movieService.buscarPelicula( this.search ).subscribe();
  }

}
